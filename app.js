const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 3000 || 3000;



app.use(cors());
app.use(express.json());


app.use('/api', require('./app/routes'))
app.listen(PORT, () => {
    console.log('API ready! PORT:', PORT)
})






