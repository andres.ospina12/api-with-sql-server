const express = require('express');
const router = express.Router();
const fs = require('fs');

const pathRouter = `${__dirname}`
const removeExtension = (fileName) => {
    return fileName.split('.').shift();
}

fs.readdirSync(pathRouter).filter((file) => {
    const route = removeExtension(file);
    const skip = ['index'].includes(route)
    if (!skip) {
        router.use(`/${route}`,require(`./${route}`))
    }
})


router.get('*',(req,res) =>{
    res.status(404);
    res.send({error: 'Route Not Found'})
})

module.exports = router;