const sql = require('mssql');
const config = require('../config/dbconfig');


async function getFavoritesPets(req, res) {
    let pool = await sql.connect(config);
    await pool.request().query("SELECT * from favoritesPets").then(response => {
        res.send(response.recordsets)
        res.status(200)
    }).catch(err => {
        res.send({ err })
        res.status(401)
    })
}

async function createPet(req, res) {
    const { id, namePet, idUser } = req.body
    let pool = await sql.connect(config)
    await pool.request().query(`INSERT INTO favoritesPets VALUES ( '${id}', '${namePet}','${idUser}')`).then(response => {
        res.send(response.recordsets)
        res.status(200)
    }).catch(err => {
        res.send({ err })
        res.status(401)
    })
}


module.exports = { getFavoritesPets, createPet }